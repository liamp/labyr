local Player = require("entity.player");
local Zombie = require("entity.zombie");
local Boss1 = require("entity.boss1");
local Door = require("door");
local Composer = require("composer");
local Util = require("util");
local Sound = require("snd");
--goodversion

local CENTER_X = display.contentCenterX;
local CENTER_Y = display.contentCenterY;

local X360_LEFTSTICK_X = 1;
local X360_LEFTSTICK_Y = 2;
local X360_LEFT_DEADZONE = 0.2;

local Game = {};

Game.InputMap = {
    PLAYER_LEFT = false,
    PLAYER_RIGHT = false,
    PLAYER_UP = false,
    PLAYER_DOWN = false,
    PLAYER_ATTACK = false,
	PLAYER_SPRINT = false,
};


Game.Player = nil;
Game.CurrentRoom = nil;
Game.Running = false;
Game.CurrentRoomIndex = 1;
Game.LivingEnemies = 0;
Game.MainDrawGroup = nil;
Game.LevelData = nil;


-- {{{ Room Data
local RoomData = {

    Crypt = { --Zombie room
        Entities = {
            { Type = Boss1, X = 600, Y = 200 },
        },

        Doors = {
            { X = 400, Y = 100 },
            { X = 700, Y = 500 }
        },

        Terrain = {
            {
                -- TODO Precompute
                NearX = CENTER_X - (896 / 2.0),
                NearY = CENTER_Y - (504 / 2.0),
                FarX  = CENTER_X + (896 / 2.0),
                FarY  = CENTER_Y + (504 / 2.0)
            }
        },

        Scale = 1.75,
        Width = 896,
        Height = 504,
        BgImg = "img/r0.png"
    },

    Corridor = { -- Narrow corridor
        Entities = {
            { Type = Zombie, X = 200, Y = 200 },
            { Type = Zombie, X = 300, Y = 200 }
        },


        Doors = {
            { X = 200, Y = 180 },
            { X = 700, Y = 180 },
        },

        Terrain = {
            {
                NearX = CENTER_X - (896 / 2.0),
                NearY = CENTER_Y - (336 / 2.0),
                FarX  = CENTER_X + (896 / 2.0),
                FarY  = CENTER_Y + (336 / 2.0),
            }
        },

        Scale = 1.75,
        Width = 512*1.75,
        Height = 192*1.75,
        BgImg = "img/r1.png"
    },

    Junction = { -- T-Junction room
        Entities = {
            { Type = Zombie, X = 200, Y = 200 },
            { Type = Zombie, X = 300, Y = 200 }
        },

        Doors = {
            { X = 200, Y = 124 },
            { X = 700, Y = 124 },
        },

        Terrain = {
            { -- Large horizontal part
                NearX = CENTER_X - (896 / 2.0),
                NearY = CENTER_Y - (280 / 2.0) - 84,
                FarX  = CENTER_X + (896 / 2.0),
                FarY  = CENTER_Y + (280 / 2.0) - 84,
            },

            { -- Small part
                NearX = CENTER_X - (336 / 2.0),
                NearY = CENTER_Y - (168 / 2.0) + 140 ,
                FarX  = CENTER_X + (336 / 2.0),
                FarY  = CENTER_Y + (168 / 2.0) + 140,
            },
        },

        Scale = 1.75,
        Width = 512*1.75,
        Height = 256*1.75,
        BgImg = "img/r2.png"
    },

    Small = { -- Dead end
        Entities = {
            { Type = Zombie, X = 500, Y = 200 },
            { Type = Zombie, X = 300, Y = 200 }
        },

        Doors = {
            { X = 500, Y = 120 },
            { X = 500, Y = 120 }
        },

        Terrain = {
            {
                NearX = CENTER_X - (448 / 2.0),
                NearY = CENTER_Y - (448 / 2.0),
                FarX  = CENTER_X + (448 / 2.0),
                FarY  = CENTER_Y + (448 / 2.0),
            }
        },

        Scale = 1.75,
        Width = 256*1.75,
        Height = 256*1.75,
        BgImg = "img/r3.png"
    },
};
-- }}}

--[[[local function GenerateLevelData()
    local MAX_ROOMS = 9;
    local MIN_ROOMS = 5;
    local ROOM_COUNT = 4;

    local RoomNameTable = {
        "Crypt",
        "Corridor",
        "Junction",
        "Small",
    };

    local LevelData = {};
    local UsedDoors = {};

    local LevelRoomCount = math.random(MIN_ROOMS, MAX_ROOMS);

    for RoomIndex = 1, LevelRoomCount do
        local RoomID = RoomNameTable[math.random(ROOM_COUNT)];

        LevelData[RoomIndex] = {};
        LevelData[RoomIndex].ID = RoomID;
        LevelData[RoomIndex].Cleared = false;
        LevelData[RoomIndex].Doors = {};

        for DoorIndex = 1, #RoomData[RoomID].Doors do
            local RoomTransitionID = nil;

            repeat
                NextRoomIndex = math.random(LevelRoomCount);
            until (not UsedDoors[NextRoomIndex] and (NextRoomIndex ~= RoomIndex))

            UsedDoors[NextRoomIndex] = true;
            LevelData[RoomIndex].Doors[DoorIndex] = { From = RoomIndex, To = NextRoomIndex };
        end

    end

    return LevelData;
end]]




-- Also check for connected graph
local function IsGraphic(Size, Sum)
    return (Sum % 2 == 0) and (Sum >= 2*(Size - 1));
end



local function GenerateLevel()
    
    -- XXX: Shadows global room data!
    local RoomData = {
        { Name = "Small", DoorCount = 1 },
        { Name = "Crypt", DoorCount = 2 },
        { Name = "Junction", DoorCount = 2 },
        { Name = "Corridor", DoorCount = 2 },
    };

    local MIN_ROOMS = 3;
    local MAX_ROOMS = 9;

    local RoomCount = math.random(MIN_ROOMS, MAX_ROOMS);

    local DoorSeq = {};
    local DoorCount = 0;

    local RoomStr = "";
    local LevelData = {};
    LevelData.Rooms = {};
    LevelData.Doors = {};
    LevelData.Cleared = {};

    repeat
        local MaxDoors = 0;
        DoorCount = 0;

        RoomStr = "";
        for RoomIndex = 1, RoomCount do
            local RoomID = math.random(#RoomData);
            DoorSeq[RoomIndex] = RoomData[RoomID].DoorCount;
            DoorCount = DoorCount + RoomData[RoomID].DoorCount;

            if (RoomData[RoomID].DoorCount > MaxDoors) then
                MaxDoors = RoomData[RoomID].DoorCount;
            end
            RoomStr = RoomStr .. ", " ..RoomData[RoomID].Name;
            LevelData.Rooms[RoomIndex] = RoomData[RoomID].Name;
            LevelData.Cleared[RoomIndex] = false;
        end
    until (IsGraphic(RoomCount, DoorCount))

    print(RoomStr);

    local MaxDoors = 0;
    local MinDoors = 100;
    local MaxIndex = 1;
    local MinIndex = 1;
    for DoorIndex = 1, DoorCount / 2 do

        print("Door #" .. DoorIndex);
        print("Min: " .. MinDoors .. "(" .. MinIndex .. "), Max: " .. MaxDoors .. "(" .. MaxIndex .. ")");

        MaxDoors = 0;
        MinDoors = 100;
        -- Find max
        for RoomIndex = 1, RoomCount do
            local R = DoorSeq[RoomIndex];

            if (R > MaxDoors) then
                MaxDoors = R;
                MaxIndex = RoomIndex;
            end
        end

        -- Find min
        for RoomIndex = 1, RoomCount do
            local R = DoorSeq[RoomIndex];

            if (R < MinDoors and R > 0 and RoomIndex ~= MaxIndex) then
                MinDoors = R;
                MinIndex = RoomIndex;
            end
        end

        DoorSeq[MinIndex] = DoorSeq[MinIndex] - 1;
        DoorSeq[MaxIndex] = DoorSeq[MaxIndex] - 1;

        print("Add door (" .. MaxIndex .. "," .. MinIndex .. ")");
        print("\n");

        LevelData.Doors[DoorIndex] = { A = MaxIndex, B = MinIndex };
    end

    return LevelData;
end



local function LoadRoom(ID, DrawGroup, Level, Index)
    local LoadedRoom = {};
    local RoomDetails = RoomData[ID];

    LoadedRoom.BgImg = display.newImage(DrawGroup, RoomDetails.BgImg, CENTER_X, CENTER_Y);
    LoadedRoom.BgImg.xScale = RoomDetails.Scale;
    LoadedRoom.BgImg.yScale = RoomDetails.Scale;

    LoadedRoom.Doors = {};
    LoadedRoom.Entities = {};

    local DoorsForThisRoom = {};
    for DoorIndex = 1, #Level.Doors do
        local D = Level.Doors[DoorIndex];

        if (D.A == Index or D.B == Index) then
            DoorsForThisRoom[#DoorsForThisRoom + 1] = D;
        end
    end

    for RoomDoor = 1, #RoomDetails.Doors do
        if (DoorsForThisRoom[RoomDoor]) then
            local DoorX = RoomDetails.Doors[RoomDoor].X;
            local DoorY = RoomDetails.Doors[RoomDoor].Y;
            local DoorA = DoorsForThisRoom[RoomDoor].A;
            local DoorB = DoorsForThisRoom[RoomDoor].B or 0;
            LoadedRoom.Doors[#LoadedRoom.Doors + 1] = Door:Create(DrawGroup, DoorX, DoorY, DoorA, DoorB);
        end
    end

    if (not Level.Cleared[Index]) then
        local RoomEntities = RoomData[ID].Entities;
        for EntityIndex = 1, #RoomEntities do
            local Entity = RoomEntities[EntityIndex];
            LoadedRoom.Entities[EntityIndex] = Entity.Type:Spawn(DrawGroup, Entity.X, Entity.Y);
        end
    end

    LoadedRoom.RoomID = ID;
    Game.LivingEnemies = #LoadedRoom.Entities;
    return LoadedRoom;
end


local function UnloadRoom(Room)
    if (Room.BgImg) then
        Room.BgImg:removeSelf();
        Room.BgImg = nil;

        for EntityIndex = 1, #Room.Entities do
            if (Room.Entities[EntityIndex]) then
                if (Room.Entities[EntityIndex].Alive) then
                    Room.Entities[EntityIndex]:Die();
                end
                Room.Entities[EntityIndex] = nil;
            end
        end

        for DoorIndex = 1, #Room.Doors do
            if (Room.Doors[DoorIndex]) then
                Room.Doors[DoorIndex]:Destroy();
                Room.Doors[DoorIndex] = nil;
            end
        end

        Room.Entities = nil;
        Room.Doors = nil;
    end
end


function Game.Over(WasVictory)
    if (Game.Running) then
        print("Set game over");
        Game.Player:Die();

        for Room = 1, #Game.LevelData do
            if (Game.LevelData[Room]) then
                UnloadRoom(Game.LevelData[Room].ID);
                print("unloaded room " .. Game.LevelData[Room].ID);
            end
        end

        Game.MainDrawGroup:removeSelf();
        Game.MainDrawGroup = nil;

        Runtime:removeEventListener("enterFrame", UpdateGame);
        Runtime:removeEventListener("key", KeyboardIput);
        Runtime:removeEventListener("axis", ControllerInput);

        Game.Running = false;
        Game.LevelData = nil;
        Game.CurrentRoom = nil;


        if (WasVictory) then
            Composer.gotoScene("ui.game_victory");
        else
            Composer.gotoScene("ui.game_over");
        end
    end
end


function Game.GetPointTraversable(X, Y)
    local Room = RoomData[Game.CurrentRoom.RoomID];

    for TerrainIndex = 1, #Room.Terrain do
        local T = Room.Terrain[TerrainIndex];

        if ((X > T.NearX and X < T.FarX) and (Y > T.NearY and Y < T.FarY)) then
            return true;
        end
    end

    return false;
end


local function TransitionRoom(NextRoomIndex)
    UnloadRoom(Game.CurrentRoom);

    Game.CurrentRoom = LoadRoom(Game.LevelData.Rooms[NextRoomIndex], Game.MainDrawGroup, Game.LevelData, NextRoomIndex);

    -- TODO Kind of a hack here tbh
    Game.Player.Sprite:toFront();
    --Game.Player.HitboxRect:toFront();
    Game.Player.Healthbar:toFront();
end


local function ControllerInput(Event)
    local Axis = Event.axis.number;

    if (Axis == X360_LEFTSTICK_X) then
        Game.InputMap.PLAYER_RIGHT = (Event.normalizedValue > X360_LEFT_DEADZONE);
        Game.InputMap.PLAYER_LEFT  = (Event.normalizedValue < -X360_LEFT_DEADZONE);
    end

    if (Axis == X360_LEFTSTICK_Y) then
        Game.InputMap.PLAYER_DOWN = (Event.normalizedValue > X360_LEFT_DEADZONE);
        Game.InputMap.PLAYER_UP   = (Event.normalizedValue < -X360_LEFT_DEADZONE);
    end
end

local function KeyboardIput(Event)
    if (Event.keyName == "w") then
        Game.InputMap.PLAYER_UP = (Event.phase == "down");
    end
    if (Event.keyName == "a") then
        Game.InputMap.PLAYER_LEFT = (Event.phase == "down");
    end
    if (Event.keyName == "s") then
        Game.InputMap.PLAYER_DOWN = (Event.phase == "down");
    end
    if (Event.keyName == "d") then
        Game.InputMap.PLAYER_RIGHT = (Event.phase == "down");
    end
	--DASHLAND-------------------------------------------------------------

	if (Event.keyName == "leftShift") then
		Game.InputMap.PLAYER_SPRINT = (Event.phase == "down");
	end

	--END OF DASHLAND--------------------------------------------------------
    if (Event.keyName == "n") then
        Game.Over(true);
    end
    if (Event.keyName == "r") then
        Game.InputMap.PLAYER_ATTACK = (Event.phase == "down");
    end

    if (Event.keyName == "buttonA") then
        Game.InputMap.PLAYER_ATTACK = (Event.phase == "down");
    end

    if (Event.keyName == "buttonB") then
        Game.InputMap.PLAYER_SPRINT = (Event.phase == "down");
    end
end

local function UpdateGame()
    if (Game.Running) then
        if (Game.Player.Health <= 0) then
            Game.Player:Die();
            Sound.Stop();
            Sound.PlayMusic("DeathTheme");
            Game.Over(false);
            return;
        end

        if (Game.LivingEnemies == 0) then Game.LevelData.Cleared[Game.CurrentRoomIndex] = true end;
        
        Game.Player:Update(Game);

        -- Update level entities
        if (not Game.LevelData.Cleared[Game.CurrentRoomIndex]) then
            for EntityIndex = 1, #Game.CurrentRoom.Entities do
                local Entity = Game.CurrentRoom.Entities[EntityIndex];

                if (Entity) then
                    if (Entity.Alive) then
                        Entity:Update(Game);
                    else
                        Game.LivingEnemies = Game.LivingEnemies - 1;
                        Game.CurrentRoom.Entities[EntityIndex] = nil;
                    end
                end
            end
        end


        if (not Game.LevelData) then return end;
        -- Update doors
        if (Game.LevelData.Cleared[Game.CurrentRoomIndex]) then
            for DoorIndex = 1, #Game.CurrentRoom.Doors do
                local D = Game.CurrentRoom.Doors[DoorIndex];

                if (not D) then return end;

                if (Util.Intersects(Game.Player.Hitbox, D.Hitbox)) then
                    Sound.Play("DoorNoise");

                    --local NextRoomID = Game.LevelData[D.ToIndex].ID;
                    local NextRoomIndex;
                    if (D.A == Game.CurrentRoomIndex) then
                        NextRoomIndex = D.B;
                    else
                        NextRoomIndex = D.A;
                    end

                    Game.CurrentRoomIndex = NextRoomIndex;

                    TransitionRoom(NextRoomIndex);

                    Game.Player:SetPosition(CENTER_X, CENTER_Y);
                end
            end
        end



    end
end




function Game.Init()
    Game.MainDrawGroup = display.newGroup();
    Game.Running = true;

    Game.LevelData = GenerateLevel();
    Game.CurrentRoomIndex = 1;

    Game.CurrentRoom = LoadRoom(Game.LevelData.Rooms[Game.CurrentRoomIndex], Game.MainDrawGroup,Game.LevelData, Game.CurrentRoomIndex);

    Game.Player = Player:Spawn(Game.MainDrawGroup, CENTER_X, CENTER_Y);
    Sound.PlayMusic("BossBackground");

    Runtime:addEventListener("enterFrame", UpdateGame);
    Runtime:addEventListener("key", KeyboardIput);
    Runtime:addEventListener("axis", ControllerInput);
end

return Game;
