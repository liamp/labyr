local Sound = {};

local Audio = require("audio");

local Effects = {
    PlayerHurt  =  Audio.loadSound("snd/PlayerHurt.wav"),
    DoorNoise   =  Audio.loadSound("snd/DoorNoise.wav"),
    ZombieDeath =  Audio.loadSound("snd/ZombieDeath.wav"),
    PlayerAttack = Audio.loadSound("snd/PlayerAttack.wav"),
    ZombieAttack = Audio.loadSound("snd/ZombAttack.wav"),
    BossHurt     = Audio.loadSound("snd/MageHurt.wav");
    BossDeath    = Audio.loadSound("snd/FFBossDeath.wav");
    BossAttack   = Audio.loadSound("snd/Fireblast.wav");
};

local Music = {
    BossBackground = Audio.loadStream("OST/BossTheme.wav");
    DeathTheme = Audio.loadStream("OST/DeathTheme.wav");
};

function Sound.Play(ID)
    local Snd = Effects[ID];
    if (Snd) then
        Audio.play(Snd);
    end
end

function Sound.PlayMusic(ID)
    local Snd = Music[ID];
    if (Snd) then
        Audio.play(Snd);
    end
end


function Sound.Stop()
    Audio.stop();    
end


return Sound;
