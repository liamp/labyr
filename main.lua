local Composer = require("composer");
local Game = require("game");

local MenuDisplayGroup = display.newGroup();


local function P(A)
    for I = 1, #A do
        print(A[I]);
    end
end


-- Also check for connected graph
local function IsGraphic(Size, Sum)
    return (Sum % 2 == 0) and (Sum >= 2*(Size - 1));
end



local function GenerateLevel()
    
    local RoomData = {
        { Name = "Small", DoorCount = 1 },
        { Name = "Crypt", DoorCount = 2 },
        { Name = "Junction", DoorCount = 2 },
        { Name = "Corridor", DoorCount = 2 },
    };

    local MIN_ROOMS = 3;
    local MAX_ROOMS = 9;

    local RoomCount = math.random(MIN_ROOMS, MAX_ROOMS);

    local DoorSeq = {};
    local DoorCount = 0;

    local RoomStr = "";

    repeat
        local MaxDoors = 0;
        DoorCount = 0;

        RoomStr = "";
        for RoomIndex = 1, RoomCount do
            local RoomID = math.random(#RoomData);
            DoorSeq[RoomIndex] = RoomData[RoomID].DoorCount;
            DoorCount = DoorCount + RoomData[RoomID].DoorCount;

            if (RoomData[RoomID].DoorCount > MaxDoors) then
                MaxDoors = RoomData[RoomID].DoorCount;
            end
            RoomStr = RoomStr .. ", " ..RoomData[RoomID].Name;
        end
    until (IsGraphic(RoomCount, DoorCount))

    print(RoomStr);

    local MaxDoors = 0;
    local MinDoors = 100;
    local MaxIndex = 1;
    local MinIndex = 1;
    for DoorIndex = 1, DoorCount / 2 do

        print("Door #" .. DoorIndex);
        print("Min: " .. MinDoors .. "(" .. MinIndex .. "), Max: " .. MaxDoors .. "(" .. MaxIndex .. ")");

        MaxDoors = 0;
        MinDoors = 100;
        -- Find max
        for RoomIndex = 1, RoomCount do
            local R = DoorSeq[RoomIndex];

            if (R > MaxDoors) then
                MaxDoors = R;
                MaxIndex = RoomIndex;
            end
        end

        -- Find min
        for RoomIndex = 1, RoomCount do
            local R = DoorSeq[RoomIndex];

            if (R < MinDoors and R > 0 and RoomIndex ~= MaxIndex) then
                MinDoors = R;
                MinIndex = RoomIndex;
            end
        end

        DoorSeq[MinIndex] = DoorSeq[MinIndex] - 1;
        DoorSeq[MaxIndex] = DoorSeq[MaxIndex] - 1;

        print("Add door (" .. MaxIndex .. "," .. MinIndex .. ")");

    print("\n");
    end
end



local function StartGame(Event)
	MenuDisplayGroup:removeSelf();
	Game.Init();
    --GenerateLevel();
end


display.setDefault("minTextureFilter", "nearest");
display.setDefault("magTextureFilter", "nearest");

native.setProperty("windowTitleText", "Labyrinthine");

local LogoImage = display.newImage(MenuDisplayGroup, "img/logo.png", display.contentCenterX, 200);
LogoImage.xScale = 2.7;
LogoImage.yScale = 2.6;

local StartText = display.newText(MenuDisplayGroup, "Start Game", display.contentCenterX, 300);
StartText:addEventListener("touch", StartGame);

