local composer = require("composer");

local scene = composer.newScene();

local img = nil;
local QuitText = nil;

local function Quit()
    native.requestExit();
end

function scene:create(event)
    local viewgroup = self.view;
    img = {};
    img = display.newImage(viewgroup, "img/game_victory.png", display.contentCenterX, 200); 
    img.xScale = 2.6;
    img.yScale = 2.6;

    QuitText = display.newText(viewgroup, "Quit", display.contentCenterX, 300);
    QuitText:addEventListener("touch", Quit);
end


function scene:show(event)

end


function scene:hide(event)

end

function scene:destroy(event)
    self.img:removeSelf();
end
scene:addEventListener("create", scene);
scene:addEventListener("destroy", scene);
scene:addEventListener("show", scene);
scene:addEventListener("hide", scene);

return scene;
