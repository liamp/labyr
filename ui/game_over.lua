local composer = require("composer");
local Game = require("game");

local scene = composer.newScene();

local img = nil;
local RetryText = nil;
local QuitText = nil;

local function Quit()
    img:removeSelf();
    --RetryText:removeSelf();
    QuitText:removeSelf();
    native.requestExit();
end

local function Retry()
    Game.Init();
end


function scene:create(event)
    local viewgroup = self.view;
    img = {};
    img = display.newImage(viewgroup, "img/game_over.png", display.contentCenterX, 200); 
    img.xScale = 2.7;
    img.yScale = 2.6;

   -- RetryText = display.newText(viewgroup, "Retry", display.contentCenterX, 250);
   -- RetryText:addEventListener("touch", Retry);

    QuitText = display.newText(viewgroup, "Quit", display.contentCenterX, 300);
    QuitText:addEventListener("touch", Quit);
end


function scene:show(event)

end


function scene:hide(event)

end

function scene:destroy(event)
    img:removeSelf();
    BackgroundImage:removeSelf();
end


scene:addEventListener("create", scene);
scene:addEventListener("destroy", scene);
scene:addEventListener("show", scene);
scene:addEventListener("hide", scene);

return scene;
