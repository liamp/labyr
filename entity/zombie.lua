local Zombie = {};

local Util = require("util");
local Sound = require("snd");

local ZB_MAX_HEALTH = 100;
Zombie.Health = ZB_MAX_HEALTH;
Zombie.EntityName = "zombie";
Zombie.Velocity = 2.0;
Zombie.Scale = 4.6;
Zombie.AttackDamage = 15;
Zombie.Attacking = false;
Zombie.Sprite = nil;
Zombie.Hitbox = nil;
Zombie.Alive = false;
Zombie.Healthbar = nil;
Zombie.AttackFrameCount = 0;
Zombie.AttackCdCount = 0;
Zombie.Moving = false;

local MAX_ATTACK_FRAMES = 8;
local ATTACK_COOLDOWN   = 10;

local ZombieSheetData = {
    frames = {
        { -- 1
            x = 0,
            y = 0,
            width = 11,
            height = 19
        },

        { -- 2
            x = 11,
            y = 0,
            width = 12,
            height = 19 
        },

        { -- 3
            x = 23,
            y = 0,
            width = 12,
            height = 19
        },

        { -- 4
            x = 35,
            y = 0,
            width = 8,
            height = 19
        },

        { -- 5
            x = 43,
            y = 0,
            width = 9,
            height = 19
        },

        { --6
            x = 0,
            y = 19,
            width = 13,
            height = 20
        },
    }
};


local ZombieAnimData = {
    {
        name = "walk",
        start = 1, 
        count = 5,
        time = 2400, 
        loopCount = 0,
        loopDirection = "forward",

    },

    {
        name = "attack",
        start = 6,
        count = 1,
        time = 800,
        loopCount = 0,
        loopDirection = "forward",
    },
}


function Zombie:Spawn(DrawGroup, X, Y)
    Instance = {};

    setmetatable(Instance, {__index = self});
    
    --Instance.Sprite = display.newImage(DrawGroup, "img/Zombie1.png", X, Y);
    local ZombieSheet = graphics.newImageSheet("img/ZbAnim.png", ZombieSheetData);
    Instance.Sprite = display.newSprite(DrawGroup, ZombieSheet, ZombieAnimData);
    Instance.Sprite.xScale = self.Scale;
    Instance.Sprite.yScale = self.Scale;
    Instance.Sprite.anchorX = 0.0;
    Instance.Sprite.anchorY = 0.0;

    -- TODO Can precompute values here
    Instance.Hitbox = {};
    Instance.Hitbox.Width = (11 * self.Scale);
    Instance.Hitbox.Height = (19 * self.Scale);
    Instance.Hitbox.Y = Y
    Instance.Hitbox.X = X;

    --[[Instance.HitboxRect = display.newRect(DrawGroup, Instance.Hitbox.X, Instance.Hitbox.Y, Instance.Hitbox.Width, Instance.Hitbox.Height);
    Instance.HitboxRect.anchorX = 0.0;
    Instance.HitboxRect.anchorY = 0.0;
    Instance.HitboxRect:setFillColor(0.0, 0.0, 0.0, 0.0);
    Instance.HitboxRect.strokeWidth = 3.0;
    Instance.HitboxRect:setStrokeColor(255, 255, 0, 255);]]

    Instance.Alive = true;
    Instance.Healthbar = display.newRect(DrawGroup, Instance.Hitbox.X, Instance.Hitbox.Y, 50*(self.Health/ZB_MAX_HEALTH), 1.4*self.Scale);
    Instance.Healthbar:setFillColor(255, 0, 0, 255);

    Instance.Sprite:setSequence("walk");
    Instance.Sprite:play();
    return Instance;
end

function Zombie:UpdateSpritePosition()
    self.Sprite.x = (self.Hitbox.X - 0.0*self.Scale);
    self.Sprite.y = (self.Hitbox.Y);

    --self.HitboxRect.x = self.Hitbox.X;
    --self.HitboxRect.y = self.Hitbox.Y;

    if (self.Moving) then
        if (self.Sprite.sequence ~= "walk") then
            self.Sprite:setSequence("walk");
            self.Sprite:play();
        end
    end

    if (self.Attacking) then
        if (self.Sprite.sequence ~= "attack") then
            self.Sprite:setSequence("attack");
            self.Sprite:play();
        end
    end
end

function Zombie:UpdateHealthbar()
    self.Healthbar.x = self.Hitbox.X + self.Hitbox.Width / 2.0;
    self.Healthbar.y = self.Hitbox.Y - 3.7*self.Scale;
    self.Healthbar.width = 50*self.Health/ZB_MAX_HEALTH;
end


-- Zombie AI: If player is within attack range, then attack.
--            Otherwise move closer to the player.
function Zombie:Update(GameState)

    if (self.Health <= 0) then
        Sound.Play("ZombieDeath");
        self:Die();
        return;
    end

    local P = GameState.Player:GetPosition();
    local V = self.Velocity;
    local DistanceToPlayer = Util.DistanceSq(self.Hitbox.X, self.Hitbox.Y, P.X, P.Y);

    self.Moving = false;
    --self.HitboxRect:setStrokeColor(255, 255, 0, 255);

    if (self.Attacking and self.AttackFrameCount < MAX_ATTACK_FRAMES) then
        self.Attacking = false;
        self.AttackFrameCount = 0;
        self.AttackCdCount = ATTACK_COOLDOWN;
    elseif (self.AttackCdCount > 0) then
        self.AttackCdCount = self.AttackCdCount - 1;
    end

    if (DistanceToPlayer > self.Hitbox.Width) then
    	if (self.Hitbox.X < P.X) then
            self.Hitbox.X = self.Hitbox.X + V;
            self.Moving = true;
    	end

    	if (self.Hitbox.Y < P.Y) then
            self.Hitbox.Y = self.Hitbox.Y + V;
            self.Moving = true;
    	end

    	if (self.Hitbox.X > P.X) then
            self.Hitbox.X = self.Hitbox.X - V;
            self.Moving = true;
    	end

    	if (self.Hitbox.Y > P.Y) then
            self.Hitbox.Y = self.Hitbox.Y - V;
            self.Moving = true;
    	end

    else
        if (not self.Attacking and self.AttackCdCount == 0) then
            self.Attacking = true;
            self.Moving = false;
        end
    end

    if (Util.Intersects(self.Hitbox, GameState.Player.SwordHitbox)) then
        if (GameState.Player.Attacking) then
            self.Health = self.Health - GameState.Player.AttackDamage;
        end
        if (self.Attacking) then
            GameState.Player:ApplyDamage(self.AttackDamage);
            Sound.Play("ZombieAttack");
        end
    end

    if (self.Attacking and Util.Intersects(self.Hitbox, GameState.Player.Hitbox)) then
        GameState.Player:ApplyDamage(self.AttackDamage);
    end

    if (Util.Intersects(self.Hitbox, GameState.Player.SwordHitbox) and GameState.Player.Attacking) then
        self.Health = self.Health - GameState.Player.AttackDamage;
    end

    if (self.Attacking) then
        --self.HitboxRect:setStrokeColor(255, 0, 0, 255);
        self.AttackFrameCount = self.AttackFrameCount + 1;
    end

    if (self.Alive) then
        self:UpdateHealthbar();
        self:UpdateSpritePosition();
    end
end

function Zombie:Die()
    if (self.Sprite) then self.Sprite:removeSelf(); end
    --if (self.HitboxRect) then self.HitboxRect:removeSelf(); end
    if (self.Healthbar) then self.Healthbar:removeSelf(); end
    self.Sprite = nil;
    self.HitboxRect = nil;
    self.Healthbar = nil;
    self.Alive = false;
end

return Zombie;
