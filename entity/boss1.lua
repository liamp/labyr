local Boss1 = {};

local Util = require("util");
local Sound = require("snd");

local SB_MAX_HEALTH = 200;
Boss1.Health = SB_MAX_HEALTH;
Boss1.EntityName = "Boss1";
Boss1.Velocity = 1.0;
Boss1.Scale = 7.0;
Boss1.AttackDamage = 15;
Boss1.Attacking = false;
Boss1.Sprite = nil;
Boss1.Hitbox = nil;
Boss1.Alive = false;
Boss1.Healthbar = nil;
Boss1.AttackFrameCount = 0;
Boss1.AttackCdCount = 0;
Boss1.Moving = false;

local MAX_ATTACK_FRAMES = 8;
local ATTACK_COOLDOWN   = 10;

local Boss1SheetData = {
    frames = {
        { -- 1
            x = 0,
            y = 0,
            width = 20,
            height = 30, 
        },

        { -- 2
            x = 20,
            y = 0,
            width = 34,
            height = 30, 
        },

        { -- 3
            x = 54,
            y = 0,
            width = 40,
            height = 30 
        },

        { -- 4
            x = 95,
            y = 0,
            width = 45,
            height = 30 
        },

        { -- 5
            x = 140,
            y = 0,
            width = 46,
            height = 30, 
        },

        { --6
            x = 0,
            y = 30,
            width = 34,
            height = 29 
        },

        { --7
            x = 34,
            y = 30,
            width = 34,
            height = 29 
        },

        { --8
            x = 68,
            y = 31,
            width = 20,
            height = 30,
        },

        { --9
            x = 88,
            y = 31,
            width = 18,
            height = 30,
        },

        { --10
            x = 106,
            y = 31,
            width = 18,
            height = 30,
        },

        { --11
            x = 124,
            y = 31,
            width = 22,
            height = 30,
        },

        { -- 12
            x = 146,
            y = 31,
            width = 21,
            height = 30
        },
        
        { -- 13
            x = 167,
            y = 31,
            width = 19,
            height = 30,
        },

        { -- 14
            x = 0,
            y = 61,
            width = 18,
            height = 30
        },
    }
};


local Boss1AnimData = {
    {
        name = "walk",
        start = 8, 
        count = 6,
        time = 2400, 
        loopCount = 0,
        loopDirection = "forward",

    },

    {
        name = "attack",
        start = 1,
        count = 8,
        time = 1200,
        loopCount = 0,
        loopDirection = "forward",
    },
}


function Boss1:Spawn(DrawGroup, X, Y)
    Instance = {};

    setmetatable(Instance, {__index = self});
    
    --Instance.Sprite = display.newImage(DrawGroup, "img/Boss1a.png", X, Y);
    local Boss1Sheet = graphics.newImageSheet("img/SBAnim.png", Boss1SheetData);
    Instance.Sprite = display.newSprite(DrawGroup, Boss1Sheet, Boss1AnimData);
    Instance.Sprite.xScale = self.Scale;
    Instance.Sprite.yScale = self.Scale;
    Instance.Sprite.anchorX = 0.0;
    Instance.Sprite.anchorY = 0.0;

    -- TODO Can precompute values here
    Instance.Hitbox = {};
    Instance.Hitbox.Width = (11 * self.Scale);
    Instance.Hitbox.Height = (19 * self.Scale);
    Instance.Hitbox.Y = Y
    Instance.Hitbox.X = X;

    --[[Instance.HitboxRect = display.newRect(DrawGroup, Instance.Hitbox.X, Instance.Hitbox.Y, Instance.Hitbox.Width, Instance.Hitbox.Height);
    Instance.HitboxRect.anchorX = 0.0;
    Instance.HitboxRect.anchorY = 0.0;
    Instance.HitboxRect:setFillColor(0.0, 0.0, 0.0, 0.0);
    Instance.HitboxRect.strokeWidth = 3.0;
    Instance.HitboxRect:setStrokeColor(255, 255, 0, 255);]]

    Instance.Alive = true;
    Instance.Healthbar = display.newRect(DrawGroup, Instance.Hitbox.X, Instance.Hitbox.Y, 50*(self.Health/SB_MAX_HEALTH), 1.4*self.Scale);
    Instance.Healthbar:setFillColor(255, 0, 0, 255);

    Instance.Sprite:setSequence("walk");
    Instance.Sprite:play();
    return Instance;
end

function Boss1:UpdateSpritePosition()
    self.Sprite.x = (self.Hitbox.X - 6.0*self.Scale);
    self.Sprite.y = (self.Hitbox.Y);

    --[[self.HitboxRect.x = self.Hitbox.X;
    self.HitboxRect.y = self.Hitbox.Y;]]

    if (self.Moving) then
        if (self.Sprite.sequence ~= "walk") then
            self.Sprite:setSequence("walk");
            self.Sprite:play();
        end
    end

    if (self.Attacking) then
        if (self.Sprite.sequence ~= "attack") then
            self.Sprite:setSequence("attack");
            self.Sprite:play();
        end
    end
end

function Boss1:UpdateHealthbar()
    self.Healthbar.x = self.Hitbox.X + self.Hitbox.Width / 2.0;
    self.Healthbar.y = self.Hitbox.Y - 3.7*self.Scale;
    self.Healthbar.width = 50*self.Health/SB_MAX_HEALTH;
end


-- Boss1 AI: If player is within attack range, then attack.
--            Otherwise move closer to the player.
function Boss1:Update(GameState)

    if (self.Health <= 0) then
        Sound.Play("BossDeath");
        self:Die();
        GameState.Over(true);
        return;
    end

    local P = GameState.Player:GetPosition();
    local V = self.Velocity;
    local DistanceToPlayer = Util.DistanceSq(self.Hitbox.X, self.Hitbox.Y, P.X, P.Y);

    self.Moving = false;
    --self.HitboxRect:setStrokeColor(255, 255, 0, 255);

    if (self.Attacking and self.AttackFrameCount < MAX_ATTACK_FRAMES) then
        self.Attacking = false;
        self.AttackFrameCount = 0;
        self.AttackCdCount = ATTACK_COOLDOWN;
    elseif (self.AttackCdCount > 0) then
        self.AttackCdCount = self.AttackCdCount - 1;
    end

    if (DistanceToPlayer > self.Hitbox.Width) then
    	if (self.Hitbox.X < P.X) then
            self.Hitbox.X = self.Hitbox.X + V;
            self.Moving = true;
    	end

    	if (self.Hitbox.Y < P.Y) then
            self.Hitbox.Y = self.Hitbox.Y + V;
            self.Moving = true;
    	end

    	if (self.Hitbox.X > P.X) then
            self.Hitbox.X = self.Hitbox.X - V;
            self.Moving = true;
    	end

    	if (self.Hitbox.Y > P.Y) then
            self.Hitbox.Y = self.Hitbox.Y - V;
            self.Moving = true;
    	end

    else
        if (not self.Attacking and self.AttackCdCount == 0) then
            self.Attacking = true;
            self.Moving = false;
        end
    end

    if (Util.Intersects(self.Hitbox, GameState.Player.SwordHitbox)) then
        if (GameState.Player.Attacking) then
            self.Health = self.Health - GameState.Player.AttackDamage;
            Sound.Play("BossHurt");
        end
        if (self.Attacking) then
            GameState.Player:ApplyDamage(self.AttackDamage);
            Sound.Play("BossAttack");
        end
    end

    if (self.Attacking and Util.Intersects(self.Hitbox, GameState.Player.Hitbox)) then
        GameState.Player:ApplyDamage(self.AttackDamage);
    end

    if (Util.Intersects(self.Hitbox, GameState.Player.SwordHitbox) and GameState.Player.Attacking) then
        self.Health = self.Health - GameState.Player.AttackDamage;
    end

    if (self.Attacking) then
        --self.HitboxRect:setStrokeColor(255, 0, 0, 255);
        self.AttackFrameCount = self.AttackFrameCount + 1;
    end

    if (self.Alive) then
        self:UpdateHealthbar();
        self:UpdateSpritePosition();
    end
	
	if (self.Health < 200) then
		self.Velocity = 2.5;
		self.AttackDamage = 30;
	end
	
end

function Boss1:Die()
    if (self.Sprite) then self.Sprite:removeSelf(); end
    --if (self.HitboxRect) then self.HitboxRect:removeSelf(); end
    if (self.Healthbar) then self.Healthbar:removeSelf(); end
    self.Sprite = nil;
    self.HitboxRect = nil;
    self.Healthbar = nil;
    self.Alive = false;
end

return Boss1;
