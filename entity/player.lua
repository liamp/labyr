local Player = {};
--goodversion
local Util = require("util");
local Sound = require("snd");

local PL_BASE_HEALTH   = 100;
local PL_BASE_VELOCITY = 6.0;
local PL_SPRINT_VELOCITY = 12.0;
local PL_BASE_DMG      = 20.0;
local SC_PLAYER        = 4.6;


Player.Health = 100;
Player.EntityName = "player";
Player.Sprite = nil;
Player.Hitbox = nil;
Player.SwordHitbox = nil;
Player.AttackDamage = 2;

Player.Alive = false;
Player.Attacking = false;
Player.Facing = "right";
Player.Moving = false;

Player.Healthbar = nil;
Player.AttackFrameCount = 0;
Player.AttackCdCount = 0;

local MAX_ATTACK_FRAMES = 8;
local ATTACK_COOLDOWN   = 16;

-- {{{ Animation data
local PlayerSheetData = {
    frames = {
        { -- 1
            x = 0,
            y = 0,
            width = 29,
            height = 27
        },

        { -- 2
            x = 29,
            y = 0,
            width = 32,
            height = 27
        },

        { -- 3
            x = 61,
            y = 0,
            width = 32,
            height = 27
        },

        { -- 4
            x = 93,
            y = 0,
            width = 32,
            height = 27
        },

        { -- 5
            x = 0,
            y = 27,
            width = 32,
            height = 27
        },

        { -- 6
            x = 32,
            y = 27,
            width = 32,
            height = 27
        },

        { -- 7
            x = 64,
            y = 27,
            width = 32,
            height = 27
        },

        { -- 8
            x = 96,
            y = 27,
            width = 32,
            height = 27
        },

        { -- 9
            x = 0,
            y = 54,
            width = 32,
            height = 27
        },

        { -- 10
            x = 32,
            y = 54,
            width = 32,
            height = 27
        },

        { -- 11
            x = 65,
            y = 55,
            width = 44,
            height = 26
        },
    }
};
-- }}}

-- {{{ Player Sequence Data
local PlayerAnimData = {
    {
        name  = "walk",
        start = 1,
        count = 10,
        time  = 1300,
        loopCount = 0,
        loopDirection = "forward"
    },

    {
        name  = "idle",
        start = 1,
        count = 1,
        time  = 4000 
    },

    {
        name = "stab",
        start = 11,
        count = 1,
        time = 800
    },
};
-- }}}

function Player:Spawn(DrawGroup, X, Y)
    Instance = {};

    setmetatable(Instance, {__index = self});

    local PlayerImageSheet = graphics.newImageSheet("img/PlayerAnim.png", PlayerSheetData);

    Instance.Sprite = display.newSprite(DrawGroup, PlayerImageSheet, PlayerAnimData);

    Instance.Sprite.xScale = SC_PLAYER;
    Instance.Sprite.yScale = SC_PLAYER;
    Instance.Sprite.anchorX = 0.0;
    Instance.Sprite.anchorY = 0.0;

    -- TODO Precompute values here
    Instance.Hitbox = {};
    Instance.Hitbox.X = X;
    Instance.Hitbox.Y = Y;
    Instance.Hitbox.Width = (12*SC_PLAYER);
    Instance.Hitbox.Height = (22*SC_PLAYER);


    Instance.SwordHitbox = {};
    Instance.SwordHitbox.X = X;
    Instance.SwordHitbox.Y = Y;
    Instance.SwordHitbox.Width = 25 * SC_PLAYER;
    Instance.SwordHitbox.Height = 3 * SC_PLAYER;

    --[[Instance.SwordHitboxRect = display.newRect(DrawGroup, Instance.SwordHitbox.X, Instance.SwordHitbox.Y, Instance.SwordHitbox.Width, Instance.SwordHitbox.Height);
    Instance.SwordHitboxRect.anchorX = 0.0;
    Instance.SwordHitboxRect.anchorY = 0.0;
    Instance.SwordHitboxRect:setFillColor(0.0, 0.0, 0.0, 0.0);
    Instance.SwordHitboxRect.strokeWidth = 3.0;
    Instance.SwordHitboxRect:setStrokeColor(255, 255, 0, 255);

    -- Hitbox Rectangle
    Instance.HitboxRect = display.newRect(DrawGroup, Instance.Hitbox.X, Instance.Hitbox.Y, Instance.Hitbox.Width, Instance.Hitbox.Height);
    Instance.HitboxRect.anchorX = 0.0;
    Instance.HitboxRect.anchorY = 0.0;
    Instance.HitboxRect:setFillColor(0.0, 0.0, 0.0, 0.0);
    Instance.HitboxRect.strokeWidth = 3.0;
    Instance.HitboxRect:setStrokeColor(255, 255, 0, 255);]]

    Instance.Alive = true;
    Instance.Healthbar = display.newRect(DrawGroup, Instance.Hitbox.X, Instance.Hitbox.Y, 50*self.Health/PL_BASE_HEALTH, 1.4*SC_PLAYER);
    Instance.Healthbar:setFillColor(255, 0, 0, 255);
    Instance.Walking = false;

    return Instance;
end

function Player:UpdateSpritePosition()
    -- NOTE: Hitbox values need to be corrected for 
    --       odd image dimensions.
    self.Sprite.x = (self.Hitbox.X - 3.0*SC_PLAYER);
    self.Sprite.y = (self.Hitbox.Y - 3.0*SC_PLAYER);

    if (self.Facing == "left") then
        self.Sprite.x = self.Sprite.x + 18.0*SC_PLAYER;
    end
    --self.HitboxRect.x = self.Hitbox.X;
    --self.HitboxRect.y = self.Hitbox.Y;

    if (self.Facing == "right") then
        self.SwordHitbox.X = (self.Hitbox.X + 14*SC_PLAYER);
        self.SwordHitbox.Y = (self.Hitbox.Y + 5*SC_PLAYER);
    else
        self.SwordHitbox.X = (self.Hitbox.X - 20*SC_PLAYER);
        self.SwordHitbox.Y = (self.Hitbox.Y + 5*SC_PLAYER);
    end

    --self.SwordHitboxRect.x = self.SwordHitbox.X;
    --self.SwordHitboxRect.y = self.SwordHitbox.Y;

    if (self.Moving) then
        if (self.Sprite.sequence ~= "walk") then
            self.Sprite:setSequence("walk");
            self.Sprite:play();
        end
    else
        if (self.Sprite.sequence ~= "idle") then
            self.Sprite:setSequence("idle");
            self.Sprite:play();
        end
    end

    if (self.Attacking) then
        if (self.Sprite.sequence ~= "stab") then
            self.Sprite:setSequence("stab");
            self.Sprite:play();
        end
    end
end


function Player:UpdateHealthbar()
    self.Healthbar.x = self.Hitbox.X + self.Hitbox.Width / 2.0;
    self.Healthbar.y = self.Hitbox.Y - 3.7*SC_PLAYER;
    self.Healthbar.width = 50*Util.Clamp(1.0, self.Health/PL_BASE_HEALTH, 0.0);
end


function Player:Update(GameState)
    local V = PL_BASE_VELOCITY;

    self.Moving = false;
    --self.SwordHitboxRect:setStrokeColor(255, 255, 0, 255);
    if (self.Attacking and self.AttackFrameCount >= MAX_ATTACK_FRAMES) then
        self.Attacking = false;
        self.AttackFrameCount = 0;
        self.AttackCdCount = ATTACK_COOLDOWN;
    elseif (self.AttackCdCount > 0) then
        self.AttackCdCount = self.AttackCdCount - 1;
    end


    if (GameState.InputMap.PLAYER_LEFT) then
        if (self.Facing == "right") then
            self.Sprite.xScale = self.Sprite.xScale * -1.0;
            self.Facing = "left";
        end

        if (GameState.GetPointTraversable((self.Hitbox.X - V), self.Hitbox.Y)) then
            self.Hitbox.X = self.Hitbox.X - V;        
            self.Moving = true;
       end
    end


    if (GameState.InputMap.PLAYER_RIGHT) then

        if (self.Facing == "left") then
            self.Sprite.xScale = self.Sprite.xScale * -1.0;
            self.Facing = "right";
        end

        if (GameState.GetPointTraversable((self.Hitbox.X + self.Hitbox.Width + V), self.Hitbox.Y)) then
            self.Hitbox.X = self.Hitbox.X + V;

            self.Moving = true;
        end

    end

    if (GameState.InputMap.PLAYER_UP) then

        if (GameState.GetPointTraversable(self.Hitbox.X, (self.Hitbox.Y - V))) then
            self.Hitbox.Y = self.Hitbox.Y - V;
        end

        self.Moving = true;
    end

    if (GameState.InputMap.PLAYER_DOWN) then

        if (GameState.GetPointTraversable(self.Hitbox.X, (self.Hitbox.Y + self.Hitbox.Height + V))) then
            self.Hitbox.Y = self.Hitbox.Y + V;
        end

        self.Moving = true;
    end
	
		--dashdahssadasfasd
	
	if (GameState.InputMap.PLAYER_SPRINT) then
		PL_BASE_VELOCITY = PL_SPRINT_VELOCITY;
	else 
		PL_BASE_VELOCITY = 6.0;
    end
	
	--superfastrunisover
	
    if (GameState.InputMap.PLAYER_ATTACK and not self.Attacking and self.AttackCdCount == 0) then

        Sound.Play("PlayerAttack");

        self.Attacking = true;
    end


    if (self.Attacking) then

        --self.SwordHitboxRect:setStrokeColor(255, 0, 0, 255);
        self.AttackFrameCount = self.AttackFrameCount + 1;

    end

    self:UpdateSpritePosition();
    self:UpdateHealthbar();
end


function Player:GetPosition()
    return { X = self.Hitbox.X, Y = self.Hitbox.Y };
end


function Player:Die()
    if (self.Sprite) then self.Sprite:removeSelf(); end
    if (self.HitboxRect) then self.HitboxRect:removeSelf(); end
    if (self.Healthbar) then self.Healthbar:removeSelf(); end
    if (self.SwordHitboxRect) then self.SwordHitboxRect:removeSelf(); end
    self.Sprite = nil;
    self.HitboxRect = nil;
    self.Healthbar = nil;
    self.Alive = false;
end


function Player:SetPosition(X, Y)
    self.Hitbox.X = X;
    self.Hitbox.Y = Y;
    self:UpdateSpritePosition();
end


function Player:ApplyDamage(Dmg)
    self.Health = self.Health - Dmg;
    Sound.Play("PlayerHurt");
end


return Player;
