local Door = {};

local Util = require("util");

local SC_DOOR = 3.4;


Door.Sprite = nil;
Door.Hitbox = nil;
Door.A = nil
Door.B = nil;

function Door:Create(DrawGroup, X, Y, A, B)
    Instance = {};

    setmetatable(Instance, { __index = self });

    Instance.Sprite = display.newImage(DrawGroup, "img/door_top.png", X, Y);
    Instance.Sprite.xScale = SC_DOOR;
    Instance.Sprite.yScale = SC_DOOR;
    
    Instance.Hitbox = {};
    Instance.Hitbox.X = Instance.Sprite.x - (Instance.Sprite.width - 13.0*SC_DOOR);
    Instance.Hitbox.Y = Instance.Sprite.y - (Instance.Sprite.height + 4.0*SC_DOOR);
    Instance.Hitbox.Width = 11 * SC_DOOR;
    Instance.Hitbox.Height = 17 * SC_DOOR;

    --[[Instance.HitboxRect = display.newRect(DrawGroup, Instance.Hitbox.X, Instance.Hitbox.Y, Instance.Hitbox.Width, Instance.Hitbox.Height);
    Instance.HitboxRect.anchorX = 0.0;
    Instance.HitboxRect.anchorY = 0.0;
    Instance.HitboxRect:setFillColor(0.0, 0.0, 0.0, 0.0);
    Instance.HitboxRect.strokeWidth = 3.0;
    Instance.HitboxRect:setStrokeColor(255, 255, 0, 255);]]

    Instance.A = A;
    Instance.B = B;
    
    return Instance;
end


function Door:Destroy()
    if (self.Sprite) then self.Sprite:removeSelf() end;
    --if (self.HitboxRect) then self.HitboxRect:removeSelf() end;

    self.Sprite = nil;
    self.Sprite = nil;
end



return Door;
