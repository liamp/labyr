local Util = {};

function Util.Intersects(A, B)
    return (A.X < (B.X + B.Width)) and ((A.X + A.Width) > B.X) and (A.Y < (B.Y + B.Height)) and ((A.Y + A.Height) > B.Y);
end

function Util.Clamp(Top, Value, Bottom)
    if (Value > Top) then return Top end;
    if (Value < Bottom) then return Bottom end;
    return Value;
end

function Util.DistanceSq(X1, Y1, X2, Y2)
    return math.pow((X2 - X1), 2) + math.pow((Y2 - Y1), 2);
end



return Util;
